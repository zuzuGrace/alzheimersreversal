class SessionsController < ApplicationController
	def create
		study = Participant.find_by(email: params[:session][:email].downcase)
		if study && study.authenticate(params[:session][:password])
			log_in study
			redirect_to study
		else
			flash.now[:error] = "Invalid LogIn"
			redirect_to root_url
		end
	end

	def destroy
		log_out
		redirect_to root_url
	end

	def alchemist_login
	end

	def alchemist_create
		alchemist = Alchemist.find_by(alchemist: params[:session][:alchemist].downcase)
		if alchemist && alchemist.authenticate(params[:session][:password])
			log_in_alchemist alchemist
			redirect_to alchemist
		else
			flash.now[:error] = "Invalid LogIn"
			redirect_to root_url
		end
	end

	def alchemist_destroy
		log_out_alchemist
		redirect_to root_url
	end
end
