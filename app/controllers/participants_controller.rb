class ParticipantsController < ApplicationController
	def create
		@participant = Participant.new(participant_params)
		if @participant.save
			#handle saving data 
			log_in @participant
			flash[:success] = "Welcome to Your Account. Please click the various options to become familiar with the application."
			redirect_to @participant
		else 
			render 'pages/start_here'
		end
	end


	def show
		@participant = Participant.find(params[:id])
	end

	def edit
		@participant = Participant.find(params[:id])
	end

	def update
		@participant = Participant.find(params[:id])
		if @participant.update(participant_params)
			flash[:success] = "Updated your preferences"
		else 
			flash[:error] = "Unable to update"
		end

	end

	private

	def participant_params
		params.require(:participant).permit(:firstname, :lastname, :email, :password, :password_confirmation)
	end
end
