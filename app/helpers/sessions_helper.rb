module SessionsHelper
	def log_in(study)
		session[:study_id] = study.id
	end

	def current_user
		if session[:study_id]
      		@current_user ||= Participant.find_by(id: session[:study_id])
    	end
	end

	def logged_in?
		!current_user.nil?
	end 

	def log_out
		session.delete(:study_id)
		@current_user = nil
	end


	def log_in_alchemist(alchemist)
		session[:alchemist_id] = alchemist.id
	end

	def current_alchemist
		if session[:alchemist_id]
      		@current_alchemist ||= Alchemist.find_by(id: session[:alchemist_id])
    	end
    end

	def log_out_alchemist
		session.delete(:alchemist_id)
		@current_user = nil
	end
end
