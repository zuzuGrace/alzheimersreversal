# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "turbolinks:load", ->
				$('.bttn-opts').click ->
					$('.app-opts').fadeOut(250)
					if @id == 'sign-up-bttn'
						$('#sign-up-participant').slideDown(500)
					else if @id == 'login-bttn'
						$('#login-participant').slideDown(500)

