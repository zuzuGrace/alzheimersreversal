# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "turbolinks:load", ->
				quizQuestions = 6

				$('.dash-opts').mouseenter ->
						$(this).css('letter-spacing', '4px')
						$(this).css("color", "#C23F97")

				$('.dash-opts').mouseleave ->
						$(this).css('letter-spacing', '1px')
						$(this).css("color", '#6E6477')

				$('.dash-opts').click -> 
						$('.main-opts').fadeOut()
						if @id == 'dash-htw'
							$('#hItW').fadeIn(500)
						else if @id == 'dash-up-diag'
							$('#upload-diag').fadeIn(500)
						else if @id == 'dash-tips'
							$('#session').fadeIn(500)

				$('.mem_bttn').click ->
					$('.memory_assess').fadeOut(250)
					$('#images').fadeOut(250)
					if @id == 'start_tutorial'
						$('#tutorial').fadeIn(550)
					else if @id == 'start_assessment'
						$('#exercise').fadeIn(550)

			

				phrase = document.getElementById('bg_mantra').innerHTML
				intro_counter = 0

			

				$('#type_mantra').focus ->
						$('#bg_mantra').css("opacity", "0.20")
						$(this).val(null)

				$('#type_mantra').keyup ->
						if intro_counter < 2
							v = $(this).val()
							if v[0] == ' '
								$(this).val(null)
								v = $(this).val()
							originalIntroString = phrase.substr(0, v.length)
							typedIntroString = v.substr(0, v.length)
							if typedIntroString.toLowerCase() == originalIntroString.toLowerCase()
								$(this).css("color", "#5c99b9")
								if typedIntroString.length == phrase.length
									intro_counter += 1
									document.getElementById('intro-counter').innerHTML = intro_counter + ' / 25'
									$(this).val(null)
							else 
								$(this).css("color", "rgba(128, 0, 0, 0.35")
						else
							$('#start_memory_session').fadeIn(500)
							$('#start_algorithm').fadeOut()

				$('#begin-session').click ->
					$('.memory_sesh').fadeOut()
					$('#memory_recall').fadeIn(500)

				recall = document.getElementById('bg_memory').innerHTML
				m_counter = 0
				

				$('#type_memory').focus ->
					$('#bg_memory').css("opacity", "0.20")
					$(this).val(null)


				$('#type_memory').keyup ->
					if m_counter < 2
						a = $(this).val()
						if a[0] == ' '
							$(this).val(null)
							a = $(this).val()
						oriString = recall.substr(0, a.length)
						typedString = a.substr(0, a.length)
						if typedString.toLowerCase() == oriString.toLowerCase()
							$(this).css("color", "#5c99b9")
							if typedString.length == recall.length
								m_counter += 1
								document.getElementById('memory-counter').innerHTML = m_counter + ' / 250'
								$(this).val(null)
						else 
							$(this).css("color", "rgba(128, 0, 0, 0.35")

				
				array1 = []
				imgs = document.getElementsByClassName('test-imgs')
				array1.push Math.floor(( Math.random() * 14) + 1) for x in [1..5]
				


				$('#start_tutorial').click ->
					$('.test-imgs').hide()
					a = imgs[array1[0]]
					b = imgs[array1[1]]
					c = imgs[array1[2]]
					d = imgs[array1[3]]
					e = imgs[array1[4]]
					document.getElementById('imgOne').src = a.src
					document.getElementById('imgTwo').src = b.src
					document.getElementById('imgThree').src = c.src
					document.getElementById('imgFour').src = d.src
					document.getElementById('imgFive').src = e.src
					$('#imgOne').removeClass('tut').addClass('selectable-img')
					$('#imgTwo').removeClass('tut').addClass('selectable-img')
					$('#imgThree').removeClass('tut').addClass('selectable-img')
					$('#imgFour').removeClass('tut').addClass('selectable-img')
					$('#imgFive').removeClass('tut').addClass('selectable-img')

				selectedImg = ''


				$('#imgOne').click ->
					$('.memory_assess').css('opacity', '0.02')
					$('.rememberal').fadeIn(100)
					selectedImg = this.id

					

				$('#imgTwo').click ->
					$('.memory_assess').css('opacity', '0.02')
					$('.rememberal').fadeIn(100)
					selectedImg = this.id

				$('#imgThree').click ->
					$('.memory_assess').css('opacity', '0.02')
					$('.rememberal').fadeIn(100)
					selectedImg = this.id

				$('#imgFour').click ->
					$('.memory_assess').css('opacity', '0.02')
					$('.rememberal').fadeIn(100)
					selectedImg = this.id

				$('#imgFive').click ->
					$('.memory_assess').css('opacity', '0.02')
					$('.rememberal').fadeIn(100)
					selectedImg = this.id







				document.getElementById('quiz-instruct').innerHTML = 'Select an image and remember it.'
				runTime = Math.floor(( Math.random() * 20) + 5)
				document.getElementById('type-remember').innerHTML = ''
				r = 0
				rtxt = 'I can remember'
				spd = 500
				rWriter = ->
					if r < rtxt.length
						document.getElementById('type-remember').innerHTML += rtxt.charAt(r)
						r++
						setTimeout rWriter, spd
					else if r >= rtxt.length
						document.getElementById('type-remember').innerHTML = ''
						r = 0
						setTimeout rWriter, spd
						return
					document.getElementById('remember-time').innerHTML = runTime
					setTimeout runTime -= 1, 10000
					if runTime < 0 
						$('.memory_assess').css('opacity', '1')
						$('#type-remember').fadeOut()
						$('#remember-time').fadeOut()
						document.getElementById('quiz-instruct').innerHTML = 'Which Image did you select?'
						runTime = 0
						r = 0
						
						$('.selectable-img').click ->
							$('#imgOne').removeClass('selectable-img')
							$('#imgTwo').removeClass('selectable-img')
							$('#imgThree').removeClass('selectable-img')
							$('#imgFour').removeClass('selectable-img')
							$('#imgFive').removeClass('selectable-img')
							if this.id == selectedImg
								alert 'clicked correctly'
								selectedImg = ''
							else 
								alert 'you can remember'
						return
					return

				#do rWriter
				setTimeout rWriter, 5000

				
						




								
					
		