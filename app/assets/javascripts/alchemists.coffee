# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


$(document).on "turbolinks:load", ->
				setTimeout ( -> 
					$('.alert-success').remove()
					return
				), 10000

				setTimeout ( -> 
					$('.alert-danger').remove()
					return
				), 10000

				setTimeout ( ->
					$('.alert-error').remove()
					return
				), 10000