class Alchemist < ApplicationRecord
	before_save {self.alchemist = alchemist.downcase}
	validates :alchemist, presence: true
	VALID_USERNAME_REGEX = /[a-z][+]alchemist/
	validates :alchemist, presence: true, length: { maximum: 255 },
                    format: { with: VALID_USERNAME_REGEX  },
                    uniqueness: { case_sensitive: false }

    has_secure_password
    validates :password, presence: true, length: { minimum: 6 }
end
