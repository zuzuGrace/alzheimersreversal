class ParticipantSessionTracker < ApplicationRecord
  belongs_to :participant
  validates :participant_id, presence: true
  validates :reported_session_number, presence: true
  validates :reported_session_day, presence: true
  validates :date_reported, presence: true
  validates :sessions_completed, presence: true
  validates :sessions_left, presence: true
  validates :total_study_sessions, presence: true
end
