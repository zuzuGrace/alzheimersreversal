class CreateAlchemists < ActiveRecord::Migration[5.2]
  def change
    create_table :alchemists do |t|
      t.string :alchemist

      t.timestamps
    end
  end
end
