class CreateParticipants < ActiveRecord::Migration[5.2]
  def change
    create_table :participants do |t|
      t.string :firstname
      t.string :lastname
      t.string :email
      t.string :subscription_id

      t.timestamps
    end
  end
end
