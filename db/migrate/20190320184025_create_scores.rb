class CreateScores < ActiveRecord::Migration[5.2]
  def change
    create_table :scores do |t|
      t.string :week_number
      t.integer :week_score
      t.references :participant, foreign_key: true

      t.timestamps
    end
  end
end
