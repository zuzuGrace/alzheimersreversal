class AddIndexToAlchemistsAlchemist < ActiveRecord::Migration[5.2]
  def change
  	add_index :alchemists, :alchemist, unique: true
  end
end
