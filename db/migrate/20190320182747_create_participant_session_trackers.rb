class CreateParticipantSessionTrackers < ActiveRecord::Migration[5.2]
  def change
    create_table :participant_session_trackers do |t|
      t.integer :reported_session_number
      t.string :reported_session_day
      t.date :date_reported
      t.integer :sessions_completed
      t.integer :sessions_left
      t.integer :total_study_sessions
      t.references :participant, foreign_key: true

      t.timestamps
    end

     # add_index :participant_session_trackers, [:participant_id, :created_at]
  end
end
