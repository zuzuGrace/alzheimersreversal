class AddPasswordDigestToAlchemists < ActiveRecord::Migration[5.2]
  def change
    add_column :alchemists, :password_digest, :string
  end
end
