# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_20_190240) do

  create_table "alchemists", force: :cascade do |t|
    t.string "alchemist"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.index ["alchemist"], name: "index_alchemists_on_alchemist", unique: true
  end

  create_table "notifications", force: :cascade do |t|
    t.string "statement"
    t.string "type"
    t.string "trigger"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "participant_session_trackers", force: :cascade do |t|
    t.integer "reported_session_number"
    t.string "reported_session_day"
    t.date "date_reported"
    t.integer "sessions_completed"
    t.integer "sessions_left"
    t.integer "total_study_sessions"
    t.integer "participant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["participant_id"], name: "index_participant_session_trackers_on_participant_id"
  end

  create_table "participants", force: :cascade do |t|
    t.string "firstname"
    t.string "lastname"
    t.string "email"
    t.string "subscription_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.index ["email"], name: "index_participants_on_email", unique: true
  end

  create_table "scores", force: :cascade do |t|
    t.string "week_number"
    t.integer "week_score"
    t.integer "participant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["participant_id"], name: "index_scores_on_participant_id"
  end

end
