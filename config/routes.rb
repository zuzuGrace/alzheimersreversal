Rails.application.routes.draw do
  root 'pages#home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/continue', to: 'pages#start_here'

  post '/create_account', to: 'participants#create'

  post '/login_participant', to: 'sessions#create'
  delete '/logout_participant', to: 'sessions#destroy'

  get '/login_alchemist', to: 'sessions#alchemist_login'
  post '/login_alchemist', to: 'sessions#alchemist_create'
  delete '/logout_alchemist', to: 'sessions#alchemist_destroy'

  resources :participants, only: [:index, :new, :create, :edit, :update, :show]
  resources :notifications
  resources :participant_session_trackers, only: [:index, :create]
  resources :scores, only: [:index, :create]

  resources :alchemists

end
